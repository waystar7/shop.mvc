<?php

class Cart
{

    /**
     * Добавление товара в корзину (сессию)
     * @param int $id
     */
    public static function addProduct($id, $qty)
    {
        $id = intval($id);

        // Пустой массив для товаров в корзине
        $productsInCart = array();

        // Если в корзине уже есть товары (они хранятся в сессии)
        if (isset($_SESSION['products'])) {
            // То заполним наш массив товарами
            $productsInCart = $_SESSION['products'];
           
        }

        // Если товар есть в корзине, но был добавлен еще раз, увеличим количество
        if (array_key_exists($id, $productsInCart)) {
            $productsInCart[$id] += $qty;
        } else {
            // Добавляем новый товар в корзину
            $productsInCart[$id] = $qty;
        }

        $_SESSION['products'] = $productsInCart;

        return self::countItems();
    }

    public static function deleteProduct($id)
    {
        $id = intval($id);
        $productsInCart = $_SESSION['products'];

        // Если товар есть в корзине, удаляем его
        if (array_key_exists($id, $productsInCart)) {
            unset($productsInCart[$id]);
        }
        $_SESSION['products'] = $productsInCart;

        return self::countItems();
    }

    public static function changeProduct($id, $sign)
    {
        $id = intval($id);
        $productsInCart = $_SESSION['products'];

        // Если товар есть в корзине - изменяем его кол-во
        if (array_key_exists($id, $productsInCart)) {
            if ($sign == 1) {
                $productsInCart[$id] ++;
            }
            elseif($sign == 0) {
                $productsInCart[$id] --;
            }
        }
        $_SESSION['products'] = $productsInCart;

        return self::countItems();
    }

    public static function clearCart()
    {
        if (isset($_SESSION['products'])) {
            unset($_SESSION['products']);
        }
    }

    /**
     * Подсчет количество товаров в корзине (в сессии)
     * @return int 
     */
    public static function countItems()
    {
        if (isset($_SESSION['products'])) {
            $count = 0;
            foreach ($_SESSION['products'] as $id => $quantity) {
                $count = $count + $quantity;
            }
            return $count;
        } else {
            return 0;
        }
    }

    public static function getProducts()
    {
        if (isset($_SESSION['products'])) {
            return $_SESSION['products'];
        }
        return false;
    }

    public static function getTotalPrice($products)
    {
        $productsInCart = self::getProducts();

        $total = 0;
        
        if ($productsInCart) {            
            foreach ($products as $item) {
                $total += $item['price'] * $productsInCart[$item['id']];
            }
        }

        return $total;
    }

}