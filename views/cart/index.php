<?php include ROOT . '/views/layouts/header.php'; ?>

<section id="cart_items">
		<div class="container">
            <div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center"><strong>Корзина</strong></h2>    			    				    				
				</div>			 		
			</div>
			<?php if ($productsInCart): ?>
                <p>Вы выбрали такие товары:</p>
			        <div class="table-responsive cart_info">
				        <table class="table table-condensed">
					        <thead>
						        <tr class="cart_menu">
                                <td width=20%></td>
                                <td class="description">Препарат</td>
                                <td class="price">Цена</td>
                                <td class="quantity">Количество</td>
                                <td class="total">Сумма</td>
                                <td></td>
						        </tr>
					        </thead>
					        <tbody>
						
                                <?php foreach ($products as $product): ?>
                                        <tr class="cart_row">
                                            <td class="cart_product">
                                                <img src="#" alt="">
                                            </td>
                                            <td class="cart_description">
                                                <h4><a href="/product/<?php echo $product['id'];?>"><?php echo $product['name'];?></a></h4>
                                                <p>ID: <?php echo $product['code'];?></p>
                                            </td>
                                            <td class="cart_price">
                                                <p><?php echo $product['price'];?> $</p>
                                            </td>
                                            <td class="cart_quantity">
                                                <div class="cart_quantity_button">
                                                    <a class="cart_quantity_up" href="/cart/change/<?php echo $product['id'];?>/1" data-id="<?php echo $product['id'];?>"> + </a>
                                                    <input class="cart_quantity_input" type="text" name="quantity" value="<?php echo $productsInCart[$product['id']];?>" autocomplete="off" size="2">
                                                    <a class="cart_quantity_down" href="/cart/change/<?php echo $product['id'];?>/0"> - </a>
                                                </div>
                                            </td>
                                            <td class="cart_total">
                                                <p class="cart_total_price"><?php echo ($productsInCart[$product['id']] * $product['price']);?> $</p>
                                            </td>
                                            <td class="cart_delete">
                                                <a class="cart_quantity_delete" href="/cart/delete/<?php echo $product['id'];?>" data-id="<?php echo $product['id'];?>"><i class="fa fa-times"></i></a>
                                            </td>
                                        </tr>
                                        
                                <?php endforeach; ?>
						<tr>
							<td class="cart_product" width=20%></td>
							<td class="cart_description"></td>
							<td class="cart_price"></td>
							<td class="cart_quantity"><p class="cart_total_price"><b>Итого:</b></p></td>
							<td class="cart_total"><p class="cart_total_price"><b><span id="total_value"><?php echo $totalPrice;?> $</span></b></p></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php else: ?>
                        <p>Корзина пуста</p>
            <?php endif; ?>
		</div>
	</section> <!--/#cart_items-->
	<?php if ($productsInCart): ?>
	<section id="do_action">
		<div class="container">
			<div class="heading">
				<a href="/"><input type="button" name="submit" class="btn btn-default get" value="Продолжить покупки"></a>
				<a href="/cart/clear"><input type="button" name="submit" class="btn btn-default get" value="Очистить корзину"></a>
				<a href="/cart/checkout"><input type="button" name="submit" class="btn btn-default get" value="Оформить заказ"></a>
				</br></br>
			</div>
		</div>
	</section><!--/#do_action-->
	<?php endif; ?>
    
<?php include ROOT . '/views/layouts/footer.php'; ?>