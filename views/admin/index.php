<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<div class="center_content">  
 
    <div id="right_wrap">
        <div id="right_content">             
            
            <section>
                <div class="container">
                    <div class="row">

                        <br/>

                        <h4>Добрый день, администратор!</h4>

                        <br/>

                        <p>Вам доступны такие возможности:</p>

                        <br/>

                        <ul>
                            <li>Управление товарами</li>
                            <li>Управление категориями</li>
                            <li>Управление заказами</li>
                        </ul>

                    </div>
                </div>
            </section> 
                        
                    
        </div>
     </div><!-- end of right content-->

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

