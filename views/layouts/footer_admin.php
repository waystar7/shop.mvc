<div class="sidebar" id="sidebar">
    <h2>Товары</h2>
    
        <ul>
            <li><a href="/admin/product" class="selected">Каталог</a></li>
            <li><a href="/admin/product/create">Добавить товар</a></li>
        </ul>
        
    <h2>Категории</h2>
    
        <ul>
            <li><a href="/admin/category">Список категорий</a></li>
            <li><a href="/admin/category/create">Добавить категорию</a></li>
        </ul> 
    
    <h2>Заказы</h2>
    
        <ul>
            <li><a href="/admin/order">Управление заказами</a></li>
        </ul>
    </div>            
    
    
    <div class="clear"></div>
    </div> <!--end of center_content-->
    
    <div class="footer">
        Copyright © 2019 <a href="https://themefurnace.com/free-templates" rel="designer">@Yurii Sokolov</a>
</div>

</div>

    	
</body>
</html>   