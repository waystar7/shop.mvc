<?php include ROOT . '/views/layouts/header_admin.php'; ?>


<div class="center_content">  
 
    <div id="right_wrap">
        <div id="right_content">         
            <h2>Список категорий</h2>

            <br/>

            <table id="rounded-corner" style="width: 50%;">
                <tr>
                    <th width="5%">ID</th>
                    <th>Название категории</th>
                    <th width="10%">#</th>
                    <th>Статус</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($categoriesList as $category): ?>
                    <tr>
                        <td><?php echo $category['id']; ?></td>
                        <td><?php echo $category['name']; ?></td>
                        <td><?php echo $category['sort_order']; ?></td>
                        <td><?php echo Category::getStatusText($category['status']); ?></td>  
                        <td><a href="/admin/category/update/<?php echo $category['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="/admin/category/delete/<?php echo $category['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            
        </div>
    </div>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

