<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<div class="center_content">  
 
    <div id="right_wrap">
        <div id="right_content">             
            <h2>Список товаров</h2>   
                    
                <table id="rounded-corner">
                    <thead>
                        <tr>
                            <th width=5%>ID</th>
                            <th width=10%>Артикул</th>
                            <th width=10%>Фото</th>
                            <th width=20%>Название товара</th>
                            <th width=5%>Цена</th>
                            <th width=10%>Наличие</th>
                            <th width=20%>Производитель</th>
                            <th>Изменить</th>
                            <th>Удалить</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($productsList as $product): ?>
                            <tr>
                                <td><?php echo $product['id']; ?></td>
                                <td><?php echo $product['code']; ?></td>
                                <td><img src="<?php echo Product::getImage($product['id']); ?>" width="75px" /></td>
                                <td><?php echo $product['name']; ?></td>
                                <td><?php echo $product['price']; ?></td>
                                <td><?php echo Product::getAvailabilityText($product['availability']); ?></td>
                                <td><?php echo $product['brand']; ?></td>    
                                <td><a href="/admin/product/update/<?php echo $product['id']; ?>" title="Редактировать"><img src="<?php ROOT; ?>/template/images/edit.png" alt="" title="" border="0" /></a></td>
                                <td><a href="/admin/product/delete/<?php echo $product['id']; ?>" title="Удалить"><img src="<?php ROOT; ?>/template/images/delete.png" alt="" title="" border="0" /></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
        </div>
     </div><!-- end of right content-->

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

