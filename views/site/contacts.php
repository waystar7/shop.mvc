<?php include ROOT . '/views/layouts/header.php'; ?>

<div id="contact-page" class="container">
    <div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-7">    			   			
					<h2 class="title text-center">Наши <strong>Контакты</strong></h2>	    				    				
					<div id="gmap" class="contact-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d20783.72152019114!2d35.43909393860836!3d49.37173162225137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1z0LMuINCa0YDQsNGB0L3QvtCz0YDQsNC0LCDRg9C7LiDQqNC40L3QtNC70LXRgNCwIDE0Mw!5e0!3m2!1sru!2sus!4v1555938562952!5m2!1sru!2sus" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>			 		
			
			
    		
	    		<div class="col-sm-5">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Наш адрес</h2>
	    				<address>
	    					<p>ООО Биотрейд Лтд.</p>
							<p>Харьковская область, г. Красноград, ул. Шиндлера 143</p>
							<p>Украина, 63304</p>
							<p>Телефон: +38 (057) 447 52 34</p>
							<p>Email: biotrade.kharkov@gmail.com</p>
	    				</address>
	    				
	    			</div>
    			</div>    			
	    	</div>  
    </div>	
</div><!--/#contact-page-->
</br>

    <?php include ROOT . '/views/layouts/footer.php'; ?>