<?php include ROOT . '/views/layouts/header.php'; ?>

<div id="contact-page" class="container">
    <div class="bg">
        <div class="row">    		
	    	<div class="col-sm-12">    			   			
				<h2 class="title text-center"><strong>Ошибка!</strong></h2>	
                                <p align="center">Такой страницы не существует. Попробуйте еще раз...</p>
                                <img src="template/images/404/404.png" align="center"/>
            </div>
        </div>
    </div>
</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>