<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<div class="center_content">  
 
    <div id="right_wrap">
        <div id="right_content">         
            <h2>Список заказов</h2>

            <br/>

            
            <table id="rounded-corner" style="width: 75%;">
                <tr>
                    <th>ID заказа</th>
                    <th>Имя покупателя</th>
                    <th>Телефон покупателя</th>
                    <th>Дата оформления</th>
                    <th>Статус</th>
                    <th>Просмотр</th>
                    <th>Изменить</th>
                    <th>Удалить</th>
                </tr>
                <?php foreach ($ordersList as $order): ?>
                    <tr>
                        <td>
                            <a href="/admin/order/view/<?php echo $order['id']; ?>">
                                <?php echo $order['id']; ?>
                            </a>
                        </td>
                        <td><?php echo $order['user_name']; ?></td>
                        <td><?php echo $order['user_phone']; ?></td>
                        <td><?php echo $order['date']; ?></td>
                        <td><?php echo Order::getStatusText($order['status']); ?></td>    
                        <td><a href="/admin/order/view/<?php echo $order['id']; ?>" title="Смотреть"><img src="<?php ROOT; ?>/template/images/view.png" alt="" title="" border="0" /></a></td>
                        <td><a href="/admin/order/update/<?php echo $order['id']; ?>" title="Редактировать"><img src="<?php ROOT; ?>/template/images/edit.png" alt="" title="" border="0" /></a></td>
                        <td><a href="/admin/order/delete/<?php echo $order['id']; ?>" title="Удалить"><img src="<?php ROOT; ?>/template/images/delete.png" alt="" title="" border="0" /></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>
    </div>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

