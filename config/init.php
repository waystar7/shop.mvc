<?php

define('CONF', ROOT . '/config');
define('CORE', ROOT . '/components');
define('MODELS', ROOT . '/models');
define('VIEWS', ROOT . '/views');

define('FRONTEND', ROOT . '/template');

date_default_timezone_set('Europe/Kiev');