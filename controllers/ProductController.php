<?php

//include_once MODELS . '/Category.php';
//include_once MODELS . '/Product.php';

class ProductController
{

    public function actionView($productId)
    {

        $categories = array();
        $categories = Category::getCategoriesList();
        
        $product = Product::getProductById($productId);

        require_once(VIEWS . '/product/view.php');

        return true;
    }

}
