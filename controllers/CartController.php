﻿<?php

class CartController
{

    public function actionAdd($id, $qty)
    {
        // Добавляем товар в корзину
        echo Cart::addProduct($id, $qty);

        // Возвращаем пользователя на страницу
        //$referrer = $_SERVER['HTTP_REFERER'];
       // header("Location: $referrer");
       return true;
    }

    public function actionDelete($id)
    {
        // Удалить товар из корзины
        Cart::deleteProduct($id);
        // Возвращаем пользователя на страницу
        header("Location: /cart");
        
    }

    public function actionChange($id, $sign)
    {
        // Изменить кол-во товара в корзине
        Cart::changeProduct($id, $sign);
        // Возвращаем пользователя на страницу
        //header("Location: /cart");
        $referrer = $_SERVER['HTTP_REFERER'];
        header("Location: $referrer");
    }

    public function actionClear()
    {
        // Очистить корзину
        Cart::clearCart();
        // Возвращаем пользователя на главную страницу
        header("Location: /");
    }

    public function actionIndex()
    {
        $categories = array();
        $categories = Category::getCategoriesList();

        $productsInCart = false;

        // Получим данные из корзины
        $productsInCart = Cart::getProducts();

        if ($productsInCart) {
            // Получаем полную информацию о товарах для списка
            $productsIds = array_keys($productsInCart);
            $products = Product::getProdustsByIds($productsIds);

            // Получаем общую стоимость товаров
            $totalPrice = Cart::getTotalPrice($products);
        }

        require_once(ROOT . '/views/cart/index.php');

        return true;
    }

    /**
     * Action для страницы "Оформление покупки"
     */
    public function actionCheckout()
    {
        // Получаем данные из корзины      
        $productsInCart = Cart::getProducts();

        // Если товаров нет, отправляем пользователи искать товары на главную
        if ($productsInCart == false) {
            header("Location: /");
        }

        // Список категорий для левого меню
        $categories = Category::getCategoriesList();

        // Находим общую стоимость
        $productsIds = array_keys($productsInCart);
        $products = Product::getProdustsByIds($productsIds);
        $totalPrice = Cart::getTotalPrice($products);

        // Количество товаров
        $totalQuantity = Cart::countItems();

        // Поля для формы
        $userName = false;
        $userPhone = false;
        $userComment = false;

        // Статус успешного оформления заказа
        $result = false;

        // Проверяем является ли пользователь гостем
        //if (!User::isGuest()) {
            // Если пользователь не гость
            // Получаем информацию о пользователе из БД
            //$userId = User::checkLogged();
            //$user = User::getUserById($userId);
            //$userName = $user['name'];
        //} else {
            // Если гость, поля формы останутся пустыми
            $userId = 0;
        //}

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Получаем данные из формы
            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];
            $userEmail = $_POST['userEmail'];
            $userComment = $_POST['userComment'];
           

            // Флаг ошибок
            $errors = false;

            // Валидация полей
            if (!User::checkName($userName)) {
                $errors[] = 'Неправильное имя';
            }
            if (!User::checkPhone($userPhone)) {
                $errors[] = 'Неправильный телефон';
            }

            if (!User::checkEmail($userEmail)) {
                $errors[] = 'Неправильный email';
            }

            if ($errors == false) {
                // Если ошибок нет
                // Сохраняем заказ в базе данных
                $result = Order::save($userName, $userPhone, $userComment, $userId, $productsInCart);
                $mess = '
            <h4>Информация о заказе</h4>
            <table style="width: 33%;">
                <tr>
                    <td>Дата заказа</td>
                    <td>'.date("Y-m-d H:i:s").'</td>
                </tr>                
                <tr>
                    <td>Имя клиента</td>
                    <td>'.$userName.'</td>
                </tr>
                <tr>
                    <td>Телефон клиента</td>
                    <td>'.$userPhone.'</td>
                </tr>
                <tr>
                    <td>E-mail клиента</td>
                    <td>'.$userEmail.'</td>
                </tr>
                <tr>
                    <td>Комментарий клиента</td>
                    <td>'.$userComment.'</td>
                </tr>
            </table>

            <h4>Товары в заказе</h4>

            <table style="width: 75%;" border="0">
                <tr>
                    <th>ID товара</th>
                    <th>Артикул товара</th>
                    <th>Название</th>
                    <th>Цена</th>
                    <th>Количество</th>
                    <th>Сумма</th>
                </tr>';
                foreach ($products as $product):
                    $mess .= '<tr>';
                        $mess .= '<td>'.$product['id'].'</td>';
                        $mess .= '<td>'.$product['code'].'</td>';
                        $mess .= '<td>'.$product['name'].'</td>';
                        $mess .= '<td>'.$product['price'].' $</td>';
                        $mess .= '<td>'.$productsInCart[$product['id']].' шт.</td>';
                        $mess .= '<td>'.$product['price'] * $productsInCart[$product['id']].' $</td>';
                    $mess .= '<tr>';
                endforeach;
            $mess .= '</table>';
            $mess .= '<h4>Сумма заказа: '.$totalPrice.' $</h4>';
                if ($result) {
                    // Если заказ успешно сохранен
                    // Оповещаем администратора о новом заказе по почте                
                    $adminEmail = 'yurii.sokolov@yahoo.com';
                    $message = $mess;
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
                    $subject = 'Новый заказ!';
                    mail($adminEmail, $subject, $message, $headers);

                    // Оповещаем покупателя о его заказе по почте                
                    $adminEmail = $userEmail;
                    $message = $mess;
                    $subject = 'Новый заказ!';
                    mail($adminEmail, $subject, $message, $headers);

                    // Очищаем корзину
                    Cart::clearCart();
                }
            }
        }

        // Подключаем вид
        require_once(ROOT . '/views/cart/checkout.php');
        return true;
    }
}