<?php

//include_once MODELS . '/Category.php';
//include_once MODELS . '/Product.php';

class SiteController
{

    public function actionIndex()
    {
         //Список категорий для левого меню
        $categories = Category::getCategoriesList();

        // Список последних товаров
        $latestProducts = Product::getLatestProducts(6);

        // Список товаров для слайдера
        $sliderProducts = Product::getRecommendedProducts();
        
        require_once(VIEWS . '/site/index.php');

        return true;
    }

    public function actionAbout()
    {
        
        require_once(VIEWS . '/site/about.php');

        return true;
    }

    public function actionContacts()
    {
        
        require_once(VIEWS . '/site/contacts.php');

        return true;
    }
    
    public function action404()
    {
        
        require_once(VIEWS . '/site/404.php');

        return true;
    }
}
